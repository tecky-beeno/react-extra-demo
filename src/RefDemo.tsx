import React, {
  useImperativeHandle,
  Component,
  memo,
  useCallback,
  useMemo,
  useState,
  Ref,
  forwardRef,
  useRef,
  useEffect,
} from 'react'
import './App.css'

function makeArray<T>(n: number, fn: (i: number) => T): T[] {
  return new Array(n).fill(0).map((_, i) => fn(i))
}

function App() {
  const [a, set_a] = useState(0)
  const [b, set_b] = useState(0)
  const [c, set_c] = useState(0)
  const inc_a = useMemo(() => () => set_a(a + 1), [a])
  const inc_b = useCallback(() => set_b(b + 1), [b])
  const inc_c = useCallback(() => set_c(c => c + 1), [])
  const allCounterRef = useRef<Array<CounterController | null>>([])
  const allCheckboxRef = useRef<Array<HTMLInputElement | null>>([])
  const d = allCounterRef.current?.map(counter => counter?.getCount())
  const refresh = useRerender()
  const n = 10
  const reset = () => {
    for (let i = 0; i < n; i++) {
      let input = allCheckboxRef.current[i]
      let count = allCounterRef.current[i]
      if (input?.checked) {
        count?.setCount(0)
        input.checked = false
      }
    }
  }
  return (
    <div>
      <button onClick={reset}>Reset</button>
      <button
        ref={e => {
          console.log('button a:', e?.textContent)
        }}
        onClick={inc_a}
      >
        {a}
      </button>
      <button onClick={inc_b}>{b}</button>
      <button onClick={inc_c}>{c}</button>
      {makeArray(n, i => (
        <div key={i}>
          <input type="checkbox" ref={e => (allCheckboxRef.current[i] = e)} />
          <ForwardRefCounter
            ref={e => {
              if (allCounterRef.current) {
                allCounterRef.current[i] = e
              }
            }}
            onChange={refresh}
          />
        </div>
      ))}
      <pre>{JSON.stringify({ a, b, c, d })}</pre>
    </div>
  )
}

function useRerender() {
  const [s, set_s] = useState(0)
  return useCallback(() => set_s(s => s + 1), [])
}

function useObjectState<T>(initial: T) {
  const [value, setValue] = useState(initial)
  const inc = useCallback(() => setValue(value => (value as any) + 1), [])
  return { value, setValue, inc }
}

type CounterController = {
  getCount(): number
  setCount(cont: number): void
}
const Counter = (
  props: { onChange?: () => void },
  ref: Ref<CounterController>,
) => {
  const { value, inc, setValue } = useObjectState(0)
  console.log('Counter ref:', ref)

  const { onChange } = props

  useImperativeHandle<CounterController, CounterController>(
    ref,
    () => ({
      getCount() {
        return value
      },
      setCount: setValue,
    }),
    [value, setValue],
  )

  useEffect(() => {
    onChange?.()
  }, [onChange, value])

  return <button onClick={inc}>{value}</button>
}
const ForwardRefCounter = forwardRef(Counter)

export default App
