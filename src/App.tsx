import React, {
  useImperativeHandle,
  Component,
  memo,
  useCallback,
  useMemo,
  useState,
  Ref,
  forwardRef,
  useRef,
  useEffect,
} from 'react'
import logo from './logo.svg'
import './App.css'

function makeArray<T>(n: number, fn: (i: number) => T): T[] {
  return new Array(n).fill(0).map((_, i) => fn(i))
}

function App() {
  const [a, set_a] = useState(0)
  const [b, set_b] = useState(0)
  const [c, set_c] = useState(0)
  const inc_a = useMemo(() => () => set_a(a + 1), [a])
  const inc_b = useCallback(() => set_b(b + 1), [b])
  const inc_c = useCallback(() => set_c(c => c + 1), [])
  const allCounterRef = useRef<Array<CounterController | null>>([])
  const d = allCounterRef.current?.map(counter => counter?.getCount())
  const refresh = useRerender()
  const reset = () => {
    allCounterRef.current?.forEach(counter => counter?.reset())
  }
  const n = 10
  return (
    <div>
      <button onClick={reset}>Reset</button>
      <button
        ref={e => {
          console.log('button a:', e?.textContent)
        }}
        onClick={inc_a}
      >
        {a}
      </button>
      <button onClick={inc_b}>{b}</button>
      <button onClick={inc_c}>{c}</button>
      {makeArray(n, i => (
        <ForwardRefCounter
          key={i}
          ref={e => {
            if (allCounterRef.current) {
              allCounterRef.current[i] = e
            }
          }}
          onChange={refresh}
        />
      ))}
      <pre>{JSON.stringify({ a, b, c, d })}</pre>
    </div>
  )
}

function useRerender() {
  const [s, set_s] = useState(0)
  return useCallback(() => set_s(s => s + 1), [])
}

function useObjectState<T>(initial: T) {
  const [value, setValue] = useState(initial)
  const inc = useCallback(() => setValue(value => (value as any) + 1), [])
  return { value, setValue, inc }
}

type CounterController = {
  getCount(): number
  reset(): void
}
const Counter = (
  props: { onChange?: () => void },
  ref: Ref<CounterController>,
) => {
  const { value, inc, setValue } = useObjectState(0)
  console.log('Counter ref:', ref)

  const { onChange } = props

  const inputRef = useRef<HTMLInputElement>(null)

  const reset = useCallback(() => {
    if (inputRef.current?.checked) {
      setValue(0)
      inputRef.current.checked = false
    }
  }, [inputRef.current, setValue])

  useImperativeHandle<CounterController, CounterController>(
    ref,
    () => ({
      getCount() {
        return value
      },
      reset,
    }),
    [value, setValue],
  )

  useEffect(() => {
    onChange?.()
  }, [onChange, value])

  return (
    <div>
      <input type="checkbox" ref={inputRef} />
      <button onClick={inc}>{value}</button>
    </div>
  )
}
const ForwardRefCounter = forwardRef(Counter)

class ClassApp extends Component {
  state = {
    a: 0,
    b: 0,
    c: 0,
  }

  inc_a = () => this.setState({ a: this.state.a + 1 })
  inc_b = () => this.setState({ b: this.state.b + 1 })
  inc_c = () => this.setState({ c: this.state.c + 1 })

  render() {
    const { a, b, c } = this.state
    return (
      <div>
        <button onClick={this.inc_a}>{a}</button>
        <button onClick={this.inc_b}>{b}</button>
        <button onClick={this.inc_c}>{c}</button>
      </div>
    )
  }
}

export default App
