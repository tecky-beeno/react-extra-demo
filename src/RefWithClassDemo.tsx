import React, {
  useImperativeHandle,
  Component,
  memo,
  useCallback,
  useMemo,
  useState,
  Ref,
  forwardRef,
  useRef,
  useEffect,
} from 'react'
import './App.css'

function makeArray<T>(n: number, fn: (i: number) => T): T[] {
  return new Array(n).fill(0).map((_, i) => fn(i))
}

function App() {
  const [a, set_a] = useState(0)
  const [b, set_b] = useState(0)
  const [c, set_c] = useState(0)
  const inc_a = useMemo(() => () => set_a(a + 1), [a])
  const inc_b = useCallback(() => set_b(b + 1), [b])
  const inc_c = useCallback(() => set_c(c => c + 1), [])
  const allCounterRef = useRef<Array<CounterController | null>>([])
  const allCheckboxRef = useRef<Array<HTMLInputElement | null>>([])
  const d = allCounterRef.current?.map(counter => counter?.getCount())
  const refresh = useRerender()
  const n = 10
  const reset = () => {
    for (let i = 0; i < n; i++) {
      let input = allCheckboxRef.current[i]
      let count = allCounterRef.current[i]
      if (input?.checked) {
        count?.setCount(0)
        input.checked = false
      }
    }
  }
  return (
    <div>
      <button onClick={reset}>Reset</button>
      <button
        ref={e => {
          console.log('button a:', e?.textContent)
        }}
        onClick={inc_a}
      >
        {a}
      </button>
      <button onClick={inc_b}>{b}</button>
      <button onClick={inc_c}>{c}</button>
      {makeArray(n, i => (
        <div key={i}>
          <input type="checkbox" ref={e => (allCheckboxRef.current[i] = e)} />
          <Counter
            counterRef={e => {
              if (allCounterRef.current) {
                allCounterRef.current[i] = e
              }
            }}
            onChange={refresh}
          />
        </div>
      ))}
      <pre>{JSON.stringify({ a, b, c, d })}</pre>
    </div>
  )
}

function useRerender() {
  const [s, set_s] = useState(0)
  return useCallback(() => set_s(s => s + 1), [])
}

type CounterController = {
  getCount(): number
  setCount(count: number): void
}

interface CounterProps {
  onChange?: () => void
  counterRef: (ref: Counter | null) => void
}
class Counter extends Component<CounterProps> implements CounterController {
  state = {
    count: 0,
  }

  getCount = () => this.state.count

  inc = () => this.setCount(this.state.count + 1)

  setCount = (count: number) => this.setState({ count })

  componentDidUpdate(prevProps: CounterProps, prevState: Counter['state']) {
    if (prevState.count !== this.state.count) {
      prevProps.onChange?.()
    }
  }

  componentDidMount() {
    console.log('did mount', this.props)
    this.props.counterRef(this)
  }
  componentDidUnmount() {
    this.props.counterRef(null)
  }
  render() {
    return <button onClick={this.inc}>{this.state.count}</button>
  }
}

export default App
