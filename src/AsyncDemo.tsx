import { Component, useEffect, useState } from 'react'

export function Demo() {
  const [url, set_url] = useState('')
  const [url_to_download, set_url_to_download] = useState('')
  const [controller, set_controller] = useState<AbortController | null>(null)
  const [result, set_result] = useState('')
  async function download() {
    if (!url_to_download) {
      return
    }
    console.log('start download:', url_to_download)
    let controller = new AbortController()
    set_controller(controller)
    try {
      let res = await fetch('http://localhost:8500' + url_to_download, {
        signal: controller.signal,
      })
      set_result(await res.text())
      console.log('finish download:', url)
    } catch (error) {
      console.log('failed download:', error)
      set_result(String(error))
    }
  }
  useEffect(() => {
    download()
  }, [url_to_download])
  useEffect(() => {
    controller?.abort()
  }, [url])
  return (
    <div>
      <h1>FC</h1>
      <input value={url} onChange={e => set_url(e.currentTarget.value)} />
      <button onClick={() => set_url_to_download(url)}>download</button>
      <p>result:{result}</p>
    </div>
  )
}

class Demo2 extends Component {
  state = {
    url: '',
    result: '',
  }
  controller?: AbortController

  setResult = (result: string) => this.setState({ result })

  download = async () => {
    this.controller?.abort()
    this.controller = new AbortController()
    let url = this.state.url
    try {
      console.log('start download:', this.state.url)
      let res = await fetch('http://localhost:8500' + url, {
        signal: this.controller.signal,
      })
      this.setResult(await res.text())
      console.log('finish download:', this.state.url)
    } catch (error) {
      console.log('failed download:', error)
      this.setResult(String(error))
    }
  }
  setUrl(url: string) {
    this.controller?.abort()
    this.setState({ url })
  }
  render() {
    return (
      <div>
        <h1>CC</h1>
        <input
          value={this.state.url}
          onChange={e => this.setUrl(e.currentTarget.value)}
        />
        <button onClick={this.download}>download</button>
        <p>result:{this.state.result}</p>
      </div>
    )
  }
}

export default Demo2
