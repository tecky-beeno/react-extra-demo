let http = require('http')

function now() {
  return new Date().toLocaleTimeString()
}

let server = http.createServer((req, res) => {
  res.setHeader('Access-Control-Allow-Origin', '*')
  res.setHeader('Access-Control-Allow-Method', '*')
  req.on('error', error => {
    console.log('req error', error)
    clearTimeout(timer)
  })
  console.log(now(), 'GET', req.url)
  let timer = setTimeout(() => {
    console.log(now(), 'GOT', req.url)
    res.write('result')
    res.end()
  }, 3000)
})

server.listen(8500, () => {
  console.log('listening on http://localhost:8500')
})
