## react-extra-demo

Demo using:
- useMemo
- useCallback
- useRef
- useImperativeHandle
- cancel fetch with useEffect

And how do equivalent functionality with class component

*This project was bootstrapped with [npm init react-ts-template](./toolkit.md).*

